﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class PointsOffers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        breadcrumbs1.AddLevel("Products", "/Products/");
        int id;

        if (!int.TryParse(Convert.ToString(Page.RouteData.Values["id"]), out id))
        {
            id = int.MinValue;
        }


        SqlDataReader dr = DB.DbFunctions.GetPointsOffers(id);

        if (dr != null && dr.HasRows)
        {
            if (dr.Read())
            {
                id = Convert.ToInt32(dr["ID"]);

                ((ITmgMasterPage)Master).PageLogo = System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"] + Functions.ConvertToString(dr["ImageFilename"]);
                breadcrumbs1.AddLevel(Convert.ToString(dr["Title"]), "/Products/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/");
                fbComments1.Href = System.Configuration.ConfigurationManager.AppSettings["baseurl"] + "Products/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                ((ITmgMasterPage)Master).PageTitle = Convert.ToString(dr["Title"]);
                ((ITmgMasterPage)Master).PageURL = "Products/" + id + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                ((ITmgMasterPage)Master).PageType = "article";
                ((ITmgMasterPage)Master).PageDescription = Convert.ToString(dr["Text"]);

                litTitle.Text = Functions.ConvertToString(dr["Title"]);
                litDate.Text = Functions.UppercaseFirst(Convert.ToDateTime(dr["DateIN"]).ToString("MMMM d, yyyy"));
                imgTopCoupon.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"] + Functions.ConvertToString(dr["ImageFilename"]);

                litText.Text = Functions.ConvertToString(dr["Text"]);

                lnkBtn.HRef = Functions.ConvertToString(dr["URL"]);
                lnkCpnImg.HRef = lnkBtn.HRef;
                lnkCpnTitle.HRef = lnkBtn.HRef;

                StringBuilder sb = new StringBuilder();
                string productURL = "Products/" + dr["ID"] + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                string imageurl = System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"] + Convert.ToString(dr["ImageFilename"]);
                string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 220).Replace("\"", " ").Trim();
                string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Title"])), 220).Replace("\"", " ").Trim();

                sb.Append("<div id=\"socialst\" style=\"margin: 5px auto;\">");
                sb.AppendFormat("    <div class=\"st_facebook_hcount item\" st_url=\"{0}{1}\" st_image=\"{2}\" st_title=\"{3}\" st_summary=\"{4}\" displayText=\"Facebook\"></div>", System.Configuration.ConfigurationManager.AppSettings["baseurl"], productURL, imageurl, title, description);
                sb.AppendFormat("    <div class=\"st_twitter_hcount item\" st_url=\"{0}{1}\" st_image=\"{2}\" st_title=\"{3}\" st_summary=\"{4}\" displayText=\"Tweet\"></div>", System.Configuration.ConfigurationManager.AppSettings["baseurl"], productURL, imageurl, title, description);
                sb.AppendFormat("    <div class=\"st_pinterest_hcount item\" st_url=\"{0}{1}\" st_image=\"{2}\" st_title=\"{3}\" st_summary=\"{4}\" displayText=\"Pinterest\"></div>", System.Configuration.ConfigurationManager.AppSettings["baseurl"], productURL, imageurl, title, description);
                sb.AppendFormat("    <div class=\"st_email_hcount item\" st_url=\"{0}{1}\" st_image=\"{2}\" st_title=\"{3}\" st_summary=\"{4}\" displayText=\"Email\"></div>", System.Configuration.ConfigurationManager.AppSettings["baseurl"], productURL, imageurl, title, description);
                sb.Append("<div class=\"clear\"></div>");
                sb.Append("</div>");
                litsocial.Text = sb.ToString();
            }
        }

        dr = DB.DbFunctions.GetPointsOffersCategories(id);
        if (dr != null)
        {
            if (dr.HasRows)
            {
                StringBuilder sb = new StringBuilder();
                while (dr.Read())
                {
                    if (sb.Length > 0)
                        sb.Append(", ");
                    sb.Append(dr["Description"]);
                }
                litTags.Text = sb.ToString();
            }
            dr.Close();
            dr.Dispose();
        }

        dr = DB.DbFunctions.GetPointsOffersPrevNext(id);
        if (dr != null)
        {
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    HtmlAnchor lnk;
                    HtmlAnchor imgLnk;
                    HtmlImage img;

                    if (Convert.ToString(dr["Type"]) == "Prev")
                    {
                        lnk = lnkPrev;
                        imgLnk = imgLnkPrev;
                        img = imgPrev;
                    }
                    else
                    {
                        lnk = lnkNext;
                        imgLnk = imgLnkNext;
                        img = imgNext;
                    }

                    img.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"] + Functions.ConvertToString(dr["Thumbnail"]);
                    lnk.HRef = "/Products/" + Convert.ToString(dr["ID"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                    imgLnk.HRef = lnk.HRef;
                }
            }
            dr.Close();
            dr.Dispose();
        }
    }
}