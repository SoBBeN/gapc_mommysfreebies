﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;

public class AzureStorage
{
    private readonly string _connectionString;
    private readonly CloudStorageAccount _cloudStorageAccount;

    public AzureStorage(string connectionString)
    {            
        _connectionString = connectionString;
        _cloudStorageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings[_connectionString].ConnectionString);
    }

    public CloudBlockBlob UploadBlog(string containerName, string folder, ref string fileName, Stream file, bool overrideIfExist = false)
    {
        var blobClient = _cloudStorageAccount.CreateCloudBlobClient();
        var container = blobClient.GetContainerReference(containerName);

        container.CreateIfNotExists();

        if(folder != string.Empty)
        {
            folder = folder + "/";
        }

        var blockBlod = container.GetBlockBlobReference(folder + fileName);

        if (!overrideIfExist)
        { 
            int i = 0;
            while (blockBlod.Exists())
            {
                int index = fileName.LastIndexOf(".");
                fileName = fileName.Substring(0, index) + (++i).ToString() + fileName.Substring(index);

                blockBlod = container.GetBlockBlobReference(folder + fileName);
            }
        }

        blockBlod.UploadFromStream(file);

        return blockBlod;
    }

    public CloudBlockBlob UploadBlog(string containerName, string folder, ref string fileName, byte[] file, bool overrideIfExist = false)
    {
        var blobClient = _cloudStorageAccount.CreateCloudBlobClient();
        var container = blobClient.GetContainerReference(containerName);

        container.CreateIfNotExists();

        if (folder != string.Empty)
        {
            folder = folder + "/";
        }

        var blockBlod = container.GetBlockBlobReference(folder + fileName);

        if (!overrideIfExist)
        {
            int i = 0;
            while (blockBlod.Exists())
            {
                int index = fileName.LastIndexOf(".");
                fileName = fileName.Substring(0, index) + (++i).ToString() + fileName.Substring(index);

                blockBlod = container.GetBlockBlobReference(folder + fileName);
            }
        }

        blockBlod.UploadFromByteArray(file, 0, file.Length);

        return blockBlod;
    }

    public string GetNonExistingFilename(string containerName, string folder, string fileName, Stream file)
    {
        var blobClient = _cloudStorageAccount.CreateCloudBlobClient();
        var container = blobClient.GetContainerReference(containerName);

        container.CreateIfNotExists();

        var blockBlod = container.GetBlockBlobReference(folder + fileName);

        int i = 0;
        while (blockBlod.Exists())
        {
            int index = fileName.LastIndexOf(".");
            fileName = fileName.Substring(0, index) + (++i).ToString() + fileName.Substring(index);

            blockBlod = container.GetBlockBlobReference(folder + fileName);
        }

        return fileName;
    }
}