﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RecipeAdd.aspx.cs" Inherits="RecipeAdd" ValidateRequest="false" %>

<%@ Register Assembly="MyClassLibrary" Namespace="MyClassLibrary.Controls" TagPrefix="CustomControl" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/start/jquery-ui.css" />
    <script src="ckeditor446/ckeditor.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#<%=txtDay.ClientID %>").datepicker({
                dateFormat: "mm/dd/yy",
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
	<style>
	  .selectable .ui-selecting { background: #FECA40; }
	  .selectable .ui-selected { background: #F39814; color: white; }
	  .selectable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	  .selectable li { margin: 3px; padding: 1px; float: left; width: 183px; height: 25px; font-size: 18px; font-weight:bold; line-height:25px; text-align: center; }
	</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3>Add/Edit <%=ITEMNAME %></h3>
    <br />
    <div runat="server" id="divMsg" class="mInfo" visible="false">
    </div>
    <div class="form" id="form">
        <table width="100%">
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblImageFile" AssociatedControlID="ImageFile" Text="Image File:" />
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="ImageFile" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator0" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="ImageFile" SetFocusOnError="true" />
                </td>
                <div style="float:right; overflow:visible; height:0px; position:relative; right:0px; top:-50px;">
                    <img runat="server" id="imgImageFile" style="max-height:225px; max-width:350px;" />
                </div>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblTitle" AssociatedControlID="txtTitle" Text="Title:" />
                </td>
                <td>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="text" Columns="100" Width="400px" />&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTitle" runat="server" ErrorMessage="<img align='absmiddle' src='images/warn.gif' /> Required"
                        ControlToValidate="txtTitle" SetFocusOnError="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblAuthor" AssociatedControlID="txtAuthor" Text="Author:" />
                </td>
                <td>
                    <asp:TextBox ID="txtAuthor" runat="server" CssClass="text" Columns="100" Width="400px" MaxLength="150" />&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblAuthorLink" AssociatedControlID="txtAuthorLink" Text="Author Link:" />
                </td>
                <td>
                    <asp:TextBox ID="txtAuthorLink" runat="server" CssClass="text" Columns="100" Width="400px" MaxLength="150" />&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblAuthorImage" AssociatedControlID="fileAuthorImage" Text="Author Image:" />
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="fileAuthorImage" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblPortions" AssociatedControlID="txtPortions" Text="Portions:" />
                </td>
                <td>
                    <asp:TextBox ID="txtPortions" runat="server" CssClass="text" Columns="100" Width="75px" />&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblTime" AssociatedControlID="txtTimeCook" Text="Time: (Minutes)" />
                </td>
                <td>
                    Prep Time: <asp:TextBox ID="txtTimePrep" runat="server" CssClass="text" Columns="6" Width="75px" />&nbsp;
                    Cook Time: <asp:TextBox ID="txtTimeCook" runat="server" CssClass="text" Columns="6" Width="75px" />&nbsp;
                    Total Time: <asp:TextBox ID="txtTimeTotal" runat="server" CssClass="text" Columns="6" Width="75px" />
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblDescription" AssociatedControlID="txtDescription" Text="Description:" />
                </td>
                <td>
	                <asp:TextBox TextMode="MultiLine" ID="txtDescription" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtDescription.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblIngredients" AssociatedControlID="txtIngredients" Text="Ingredients:" />
                </td>
                <td>
	                <asp:TextBox TextMode="MultiLine" ID="txtIngredients" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtIngredients.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblMethod" AssociatedControlID="txtMethod" Text="Method:" />
                </td>
                <td>
	                <asp:TextBox TextMode="MultiLine" ID="txtMethod" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtMethod.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblMeal" AssociatedControlID="sltMeal" Text="Meal:" />
                </td>
                <td>
                    <hr />
                    <CustomControl:Selectable ID='sltMeal' runat="server"></CustomControl:Selectable>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblCuisine" AssociatedControlID="sltCuisine" Text="Cuisine:" />
                </td>
                <td style="width:800px;">
                    <hr />
                    <CustomControl:Selectable ID='sltCuisine' runat="server"></CustomControl:Selectable>
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblCategory" AssociatedControlID="sltCategory" Text="Other Categories:" />
                </td>
                <td>
                    <hr />
                    <CustomControl:Selectable ID='sltCategory' runat="server"></CustomControl:Selectable>
                    <div style="clear:both;"></div>
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblActiveAll" AssociatedControlID="chkActiveAll" Text="Active Status (All Websites):" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkActiveAll" Checked="false" /> (When unchecked, only visible on dev)
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblActive" AssociatedControlID="chkActive" Text="Active Status (This Website):" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkActive" Checked="false" /> (When unchecked, only visible on dev)
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDay" AssociatedControlID="txtDay" Text="Active Date:" />
                </td>
                <td>
                    <asp:TextBox ID="txtDay" runat="server" CssClass="text" Columns="10" MaxLength="10" />
                </td>
            </tr>
        </table>

        <asp:HiddenField ID="hidID" runat="server" />
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="submit" />
        <asp:Button ID="btnSaveAdd" runat="server" Text="Save & Add New" OnClick="btnSaveAdd_Click" CssClass="submit" /> &nbsp; &nbsp; &nbsp; 
        <a target="_blank" runat="server" id="lnkPreview" visible="false">See Preview</a>
    </div>
</asp:Content>

