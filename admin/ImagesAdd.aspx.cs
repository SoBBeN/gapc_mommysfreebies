﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ImagesAdd : System.Web.UI.Page
{
    protected const string ITEMNAME = "Image";

    protected void Page_Load(object sender, EventArgs e)
    {
        divMsg.Visible = false;

        if (!IsPostBack)
        {
            FillDdl();
            if (Request.QueryString["id"] != null) //UPDATE MODE
            {
                hidID.Value = Request.QueryString["id"]; //Saving the ID to use later
                ShowExistingValues();
            }
        }
    }

    private void FillDdl()
    {
        SqlDataReader dr = DB.DbFunctions.GetImageTypes(false);
        ddlType.DataSource = dr;
        ddlType.DataTextField = "Description";
        ddlType.DataValueField = "TypeID";
        ddlType.DataBind();
        dr.Close();
    }

    private void ShowExistingValues()
    {
        SqlDataReader dr = DB.DbFunctions.GetImage(int.Parse(hidID.Value));

        if (dr.HasRows)
        {
            dr.Read();
            //(ddlType.SelectedValue), txtTitle.Text, filename, 
            RequiredFieldValidator0.Enabled = false;
            ddlType.SelectedValue = Convert.ToString(dr["Type"]);
            txtTitle.Text = Convert.ToString(dr["Title"]);
            txtText.Text = Convert.ToString(dr["Text"]);
            chkActive.Checked = Convert.ToBoolean(dr["IsActive"]);
            txtDay.Text = Functions.ConvertToString(dr["ActiveDate"]);
            imgImageFile.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "images/" + Functions.ConvertToString(dr["ImageFilename"]);
        }
        else //can't find the poll.. change to INSERT MODE
        {
            hidID.Value = String.Empty;
            btnSave.Text = "Save " + ITEMNAME;
        }
        dr.Close();
    }


    protected void btnSaveAdd_Click(object sender, EventArgs e)
    {
        btnSave_Click(sender, e);
        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int id = 0;
            string filename = String.Empty;

            if (ImageFile.HasFile)
            {
                filename = Functions.RemoveSpecialChars(ImageFile.FileName);

                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("themommyguide", "images/images", ref filename, ImageFile.PostedFile.InputStream);
            }

            string activeDate;
            if (txtDay.Text.Length > 7)
                activeDate = txtDay.Text;
            else
                activeDate = null;

            if (hidID.Value.Length == 0) //INSERT
            {
                if (filename.Length > 0)
                {
                    id = DB.DbFunctions.InsertImage(Convert.ToInt16(ddlType.SelectedValue), txtTitle.Text, filename, chkActive.Checked ? 1 : 0, txtText.Text, activeDate, null, null);

                    //POLL INSERTED SUCCESSFULLY
                    if (id > 0)
                    {
                        hidID.Value = id.ToString();
                        divMsg.InnerHtml = "Your new " + ITEMNAME + " has been created successfully.";
                        divMsg.Visible = true;
                    }
                }
            }
            else //UPDATE
            {
                id = int.Parse(hidID.Value);
                DB.DbFunctions.UpdateImage(id, Convert.ToInt16(ddlType.SelectedValue), txtTitle.Text, filename, chkActive.Checked ? 1 : 0, txtText.Text, activeDate, null, null);

                divMsg.InnerHtml = "Your " + ITEMNAME + " has been updated successfully.";
                divMsg.Visible = true;
            }
        }
    }

}