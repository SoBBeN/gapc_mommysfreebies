﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Lifestyle : System.Web.UI.Page
{
    protected const string ITEMNAME = "Luck Letter Article";

    protected void Page_Load(object sender, EventArgs e)
    {
        divMsg.Visible = false;
        if (!IsPostBack)
        {
            FillCategoryFilter();
            FillGV();
        }
    }

    private void FillGV()
    {
        SqlDataReader dt = DB.DbFunctions.GetAllLifestyle();

        gv.DataSource = dt;
        gv.DataBind();
    }

    private void FillCategoryFilter()
    {
        SqlDataReader dt = DB.DbFunctions.GetLifestyleCategoryDistinct();
        ddlCategories.DataSource = dt;
        ddlCategories.DataValueField = "ID";
        ddlCategories.DataTextField = "Description";
        ddlCategories.DataBind();

        ddlCategories.Items.Insert(0, new ListItem("", "0"));
    }
    protected void ddlCategories_Change(object sender, EventArgs e)
    {
        SqlDataReader dt;

        if (ddlCategories.SelectedValue == "0")
        {
            dt = DB.DbFunctions.GetAllLifestyle();
        }
        else
        {
             dt = DB.DbFunctions.LifestyleGetByCategory(int.Parse(ddlCategories.SelectedValue));
        }

        gv.DataSource = dt;
        gv.DataBind();
    }

    protected void GvRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gv.DataKeys[e.RowIndex]["ID"].ToString());

        DB.DbFunctions.DeleteLifestyle(id);

        divMsg.InnerHtml = "Selected " + ITEMNAME + " deleted successfully.";
        divMsg.Visible = true;

        FillGV();
    }
  
}