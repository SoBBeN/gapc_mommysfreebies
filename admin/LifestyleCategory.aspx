﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LifestyleCategory.aspx.cs" Inherits="LifestyleCategory" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="divFilter">
        <form id="filter-form">
            Filter: &nbsp; 
            <input type="text" name="filter" id="filter" value="" maxlength="30" size="30" />
            <input type="button" id="clearFilter" value="Clear" />
        </form>
    </div>
    <h3><%=ITEMNAME %></h3>
        <br />
        <a href="LifestyleCategoryAdd.aspx">Add New <%=ITEMNAME %></a>
    <br />

        <div runat="server" id="divMsg" class="mInfo" visible="false">
        </div>
        <asp:GridView ID="gv" runat="server" Width="100%" GridLines="None" DataKeyNames="ID"
            OnRowDeleting="GvRowDeleting" CssClass="grid">
            <HeaderStyle CssClass="gridHead" />
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="ID" DataNavigateUrlFormatString="LifestyleCategoryAdd.aspx?id={0}"
                    Text="Edit" />
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Delete"
                            Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this lifestyle category?')" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

</asp:Content>
