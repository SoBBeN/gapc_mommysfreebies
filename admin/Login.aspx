﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<style type="text/css">
    td {
        font-size:16px;
        font-weight:bold;
        vertical-align:middle;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table style="width:280px;">
        <tr>
            <td colspan="4" align="center">
                <asp:Label ID="errMsg" runat="server" ForeColor="red" Font-Names="arial" Font-Size="10" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td>Name &nbsp;</td>
            <td style="text-align:right;">
                <asp:TextBox CssClass="textboxstyle" ID="tbUsername" Width="145" runat="server" /></td>
            <td></td>
            <td>
                <asp:RequiredFieldValidator Font-Names="Arial" Font-Size="14" Font-Bold="True" runat="server" ControlToValidate="tbUserName"
                    ErrorMessage="&nbsp;*" ID="Requiredfieldvalidator1" /></td>
        </tr>
        <tr>
            <td>Password &nbsp;</td>
            <td style="text-align:right;">
                <asp:TextBox CssClass="textboxstyle" ID="tbPassword" Width="145" runat="server" TextMode="Password" /></td>
            <td></td>
            <td>
                <asp:RequiredFieldValidator Font-Names="Arial" Font-Size="14" Font-Bold="True" runat="server" ControlToValidate="tbPassWord"
                    ErrorMessage="&nbsp;*" ID="Requiredfieldvalidator2" /></td>
        </tr>
        <tr>
            <td colspan="2" align="right" style="white-space:nowrap; font-size:12px;">
                <label for="ContentPlaceHolder1_chk_cookie">Remember my username and password</label>
            </td><td>
                <asp:CheckBox ID="chk_cookie" runat="server" Checked="True"></asp:CheckBox></td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                <asp:LinkButton ID="btLogin" runat="server" Text="Login" OnClick="btLogin_Click" CssClass="button"></asp:LinkButton></td>
        </tr>
    </table>

</asp:Content>

