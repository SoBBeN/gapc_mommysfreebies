﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Recipe.aspx.cs" Inherits="Recipe" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>
<%@ Register TagPrefix="fbc" TagName="FbComments" Src="~/UserControls/FbComments.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">var switchTo5x = true;</script>
    <script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({ publisher: "98c30075-93f7-4baa-9669-a5a1792d12a3", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
    <link href="/css/MF_Recipe.css" rel="stylesheet" />

    <script type="text/javascript">
        window.onclick = function (event) {
            if (!event.target.matches('.dropbtn')) {

                var dropdowns = document.getElementsByClassName("categories-contents");
                var i;
                for (i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (openDropdown.classList.contains('show')) {
                        openDropdown.classList.remove('show');
                    }
                }
            }
        }

        function OpenCategories(id) {
            document.getElementById("categoryDropdown").classList.toggle("show");
        }

        $(document).ready(function () {
            if (matchMedia('only screen and (min-width: 310px) and (max-width:500px)').matches) {
                //$('.ads_padding_middle').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- Free Samples - Bottom --><ins class="adsbygoogle" style="display:block;width:98%;height:90px" data-ad-client="ca-pub-0634471641041185" data-ad-slot="7335364784"></ins><script>        (adsbygoogle = window.adsbygoogle || []).push({});<\/script>');
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <div class="recipe_categories">
        <div class="breadcrumbs"><bc:Breadcrumbs id="breadcrumbs1" runat="server" /></div>
        <div class="categories">
            <img class="dropbtn" src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/MF_BrowseRecipes.png" onclick="OpenCategories()" />
        </div>
    </div>
    
    <div id="topcoupon" class="clear">
        <div id="categoryDropdown" class="categories-contents">
            <asp:Literal runat="server" ID="litCategories"></asp:Literal>
            <div class="clear all_categories">
                <hr />
                <a href="/Recipes">All Categories</a>
            </div>
        </div>
        <div class="title"><asp:Literal runat="server" ID="litTitle" /></div>
        <div class="date"><asp:Literal runat="server" ID="litDate" /></div>
        <div class="tags">Categories: <asp:Literal runat="server" ID="litTags" /></div>
        <div><asp:Literal runat="server" ID="litAuthorImage" /></div>
        <div class="date"><h3 class="author"><asp:Literal runat="server" ID="litAuthor"></asp:Literal></h3></div>
        <div class="image">
            <img runat="server" id="imgMain" class="imgTopCoupon" />
        </div>
        <div class="text">
            <div class="others"><asp:Literal runat="server" ID="litOthers"></asp:Literal></div>
            <div class="subtitle">Ingredients</div>
            <div class="ckedit"><asp:Literal runat="server" ID="litIngredients"></asp:Literal></div>
            <div class="subtitle">Directions</div>
            <div class="ckedit"><asp:Literal runat="server" ID="litMethod"></asp:Literal></div>
        </div>
        <br /><br />
        <div id="socialst">
            <span class='st_facebook_hcount item' displayText='Facebook'></span>
            <span class='st_twitter_hcount item' displayText='Tweet'></span>
            <span class='st_pinterest_hcount item' displayText='Pinterest'></span>
            <span class='st_email_hcount item' displayText='Email'></span>
        </div>
    </div>
    <div style="clear:both;"></div>
    <br /><br />
    <div id="fbCommentsPlaceholder">
        <fbc:FbComments id="fbComments1" runat="server" Width="580" />
    </div>
</asp:Content>
