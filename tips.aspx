﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="tips.aspx.cs" Inherits="tips" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../css/MF_ParentTips.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
           <bc:Breadcrumbs id="breadcrumbs1" runat="server" />

    <div class="container_parent_tips">        <h1>Parenting Tips</h1>        <hr class="blueline" />        <asp:Literal runat="server" ID="litTips"></asp:Literal>    </div></asp:Content>

