/**
 * @extends storeLocator.StaticDataFeed
 * @constructor
 */
function MedicareDataSource() {
  $.extend(this, new storeLocator.StaticDataFeed);

  var that = this;
  $.get('Daycare.aspx', function (data) {
    that.setStores(that.parse_(data));
  });
}

/**
 * @const
 * @type {!storeLocator.FeatureSet}
 * @private
 */
MedicareDataSource.prototype.FEATURES_ = new storeLocator.FeatureSet(
  new storeLocator.Feature('B-YES', 'Babies'),
  new storeLocator.Feature('P-YES', 'Preschools'),
  new storeLocator.Feature('A-YES', 'Afterschools')
);

/**
 * @return {!storeLocator.FeatureSet}
 */
MedicareDataSource.prototype.getFeatures = function() {
  return this.FEATURES_;
};

/**
 * @private
 * @param {string} csv
 * @return {!Array.<!storeLocator.Store>}
 */
MedicareDataSource.prototype.parse_ = function(csv) {
  var stores = [];
  var rows = csv.split('\n');
  var headings = this.parseRow_(rows[0]);

  for (var i = 1, row; row = rows[i]; i++) {
    row = this.toObject_(headings, this.parseRow_(row));
     
    var features = new storeLocator.FeatureSet;
    features.add(this.FEATURES_.getById('B-' + row.Baby));
    features.add(this.FEATURES_.getById('P-' + row.Preschool));
    features.add(this.FEATURES_.getById('A-' + row.Afterschool));

    var position = new google.maps.LatLng(row.Latitude, row.Longitude);

    var locality = this.join_([row.City, row.State, row.ZIP], ', ');

    var store = new storeLocator.Store(row.ID, position, features, {
        title: '<a target="_blank" href="' + row.Website + '">' + row.Name + '</a>',
      address: this.join_([row.Address, locality], '<br>'),
      phone: parsePhone(row.Phone),
      web: parseWebsite(row.Website)
    });
    stores.push(store);
  }
  return stores;
};

function parsePhone(phone) {
    if (phone.length == 10)
        return "Phone: (" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + "-" + phone.substring(6, 10);
    else
        return phone;
}

function parseWebsite(web) {
    if (web.length > 0)
        return '<a target="_blank" href="' + web + '">' + web + '</a>';
    else
        return web;
}

/**
 * Joins elements of an array that are non-empty and non-null.
 * @private
 * @param {!Array} arr array of elements to join.
 * @param {string} sep the separator.
 * @return {string}
 */
MedicareDataSource.prototype.join_ = function(arr, sep) {
  var parts = [];
  for (var i = 0, ii = arr.length; i < ii; i++) {
    arr[i] && parts.push(arr[i]);
  }
  return parts.join(sep);
};

/**
 * Very rudimentary CSV parsing - we know how this particular CSV is formatted.
 * IMPORTANT: Don't use this for general CSV parsing!
 * @private
 * @param {string} row
 * @return {Array.<string>}
 */
MedicareDataSource.prototype.parseRow_ = function(row) {
  // Strip leading quote.
  if (row.charAt(0) == '"') {
    row = row.substring(1);
  }
  // Strip trailing quote. There seems to be a character between the last quote
  // and the line ending, hence 2 instead of 1.
  if (row.charAt(row.length - 2) == '"') {
    row = row.substring(0, row.length - 2);
  }

  row = row.split('","');

  return row;
};

/**
 * Creates an object mapping headings to row elements.
 * @private
 * @param {Array.<string>} headings
 * @param {Array.<string>} row
 * @return {Object}
 */
MedicareDataSource.prototype.toObject_ = function(headings, row) {
  var result = {};
  for (var i = 0, ii = row.length; i < ii; i++) {
    result[headings[i]] = row[i];
  }
  return result;
};
