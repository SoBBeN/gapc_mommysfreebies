﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.
[ServiceContract(Namespace = "TMG")]
public interface IService
{

	[OperationContract]
	string GetData(int value);

    [OperationContract]
    CompositeType GetDataUsingDataContract(CompositeType composite);
    
    [OperationContract]
    void InsertNewsLetterEmail(string email);

    [OperationContract]
    FbPageDaySerializable GetFbPageDay(int id, short diff);

    [OperationContract]
    TodaysMomSerializable GetTodaysMom(int id, short diff);

    [OperationContract]
    BlogSerializable GetBlog(int id, short diff, int author);

    [OperationContract]
    string GetTodaysMoms(int id, short diff);

    [OperationContract]
    string GetPhotos(int id, short diff, int type);

    [OperationContract]
    string GetMOMents(int id, short diff, int type);

    [OperationContract]
    PhotoSerializable GetPhoto(int id, short diff, int typeid);

    [OperationContract]
    ArticleSerializable GetArticle(int id, short diff, string category);

    // TODO: Add your service operations here
}

// Use a data contract as illustrated in the sample below to add composite types to service operations.
[DataContract]
public class CompositeType
{
	bool boolValue = true;
	string stringValue = "Hello ";

	[DataMember]
	public bool BoolValue
	{
		get { return boolValue; }
		set { boolValue = value; }
	}

	[DataMember]
	public string StringValue
	{
		get { return stringValue; }
		set { stringValue = value; }
	}
}
