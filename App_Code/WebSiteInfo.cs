﻿using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for SearchWebSite
/// </summary>
public class WebSiteInfo
{
	private int WebSiteID;
	private string WebSiteBaseUrl;  //Used to hold the base URL
	private string SiteCode;
	private string Title; //used to determine what css to use and to set title
	private string ContentHeadCssFile;
	private string MasterPageHead;
	private string Disclaimer;
	private string Privacy;
	private string Terms;
	private string HeaderText;
	private string FacebookAppId;
	private DB_Functions DateBaseReader;




	

	//Constructor
	public WebSiteInfo(string host)
	{
		WebSiteBaseUrl = host;
		//if (WebSiteBaseUrl == "Content.Click2WinForLife.com" || WebSiteBaseUrl == "gapc-content-site-staging.azurewebsites.net" || WebSiteBaseUrl == "gapc-content-site-release.azurewebsites.net" || WebSiteBaseUrl == "localhost" || WebSiteBaseUrl == "gapc-content-site.azurewebsites.net" || WebSiteBaseUrl == "content.click2win4life.com")
		//{
		//	Title = "Click2WinForLife";
		//	SiteCode = "C2W";
		//	WebSiteID = 12;
			 
		//}
		
		StringBuilder sb = new StringBuilder();
		sb.AppendFormat("<link href=\"/css/{0}_news.css\" rel=\"stylesheet\" />\n", SiteCode);
		sb.AppendFormat("<link href=\"/css/{0}_lifestyle.css\" rel=\"stylesheet\" />\n", SiteCode);
		sb.AppendFormat("<link href=\"/css/{0}_jobs.css\" rel=\"stylesheet\" />\n", SiteCode);
		ContentHeadCssFile = sb.ToString();
		sb.Clear();
		//Get MasterPage Head Info
		sb.AppendFormat("<link rel=\"image_src\" href=\"https://gapc.blob.core.windows.net/gapc-content-site/images/{0}_logo.png\" />\n", SiteCode);
		sb.AppendFormat("	<link href = \"/css/{0}_style.css\" rel = \"stylesheet\" />", SiteCode).AppendLine();
		MasterPageHead = sb.ToString();
		sb.Clear();
		DateBaseReader = new DB_Functions(WebSiteID);
	}
	
	//Get WebsiteID
	public int GetWebSiteID() {return WebSiteID;}

	//Get WebSite Base URL EX: "Content.Click2WinForLife.com"
	public string GetWebSiteBaseUrl(){return WebSiteBaseUrl;}

	//Get Site Code
	public string GetSiteCode(){return SiteCode;}

	//Get Title
	public string GetTitle(){return Title;}

	//GET Info for MasterPage page head section
	public string GetMasterPageHead(){return MasterPageHead;}

	//GET Info for Default.aspx page head section
	public string GetContentHeadCssFile() {return ContentHeadCssFile;}

	//GET Disclaimers
	public string GetDisclaimer(int DisclaimerNumber) {
		var WebSiteRow = DateBaseReader.GetWebSite();
		while (WebSiteRow.Read()) {
			Disclaimer = Convert.ToString(WebSiteRow["Disclaimer" + DisclaimerNumber]).ToString();
		}

        return Disclaimer;
	}
	//GET Privacy
	public string GetPrivacy()
	{
		var WebSiteRow = DateBaseReader.GetWebSite();
		while (WebSiteRow.Read())
		{
			Privacy = Convert.ToString(WebSiteRow["Privacy"]).ToString();
		}
		return Privacy;
	}
	//GET Terms
	public string GetTerms()
	{
		var WebSiteRow = DateBaseReader.GetWebSite();
		while (WebSiteRow.Read())
		{
			Terms = Convert.ToString(WebSiteRow["Terms"]).ToString();
		}
		return Terms;
	}
	//GET Header Test
	public string GetHeaderText()
	{
		var WebSiteRow = DateBaseReader.GetWebSite();
		while (WebSiteRow.Read())
		{
			HeaderText = Convert.ToString(WebSiteRow["ContentHeader"]).ToString();
		}
		return HeaderText;
	}
	//GET Logo
	public string Getlogo()
	{
		switch (WebSiteID)
		{
			case 12:
				return "<img src=\"" + System.Configuration.ConfigurationManager.AppSettings["AssetPath"] + "/images/" + SiteCode +"_logo.png\" />";
			default:
				Console.WriteLine("Default case");
				break;
		}
		
		return "";
	}
		//Get FaceBook URL
	public string GetFaceBookURL()
	{
		switch (WebSiteID)
		{
			case 12:
				return "https://www.facebook.com/Click2Win4Life";
			default:
				Console.WriteLine("Default case");
				break;
		}
		return "";
	}
	//Get FaceBook Widget
	public string GetFaceBookWidget()
	{
				return "<div class=\"fb-page\" data-href=\""+ GetFaceBookURL() + "\" data -width=\"340\" data-hide-cover=\"false\" data-show-facepile=\"true\" data-show-posts=\"false\"></div>";
	}

	//Get FaceBook AppID
	public string GetFaceBookAppID()
	{
		switch (WebSiteID)
		{
			case 12:
				return "401722663617031";
			default:
				Console.WriteLine("Default case");
				break;
		}
		return "";
	}
	//Get FaceBook App Secret
	public string GetFaceBookAppSecretCode()
	{
		switch (WebSiteID)
		{
			case 12:
				return "9916d030d33ed7acb0a073f6764e0b92";
			default:
				Console.WriteLine("Default case");
				break;
		}
		return "";
	}
	//Get meta Keywords
	public string GetKeyWords()
	{
		switch (WebSiteID)
		{
			case 12:
				return "Win,Click,Prize,Cash";
			default:
				Console.WriteLine("Default case");
				break;
		}
		return "";
	}
	//Get Favicon
	public string GetFaviconImageName(){return SiteCode + ".ico";}
	//Get Analytic Code
	public string GetAnalyticName()
	{
		switch (WebSiteID)
		{
			case 12:
				return "c2wAnalytics";
			default:
				Console.WriteLine("Default case");
				break;
		}
		return "";
	}
    //Get signUp URL Code
    public string GetSignUPURL()
    {
        switch (WebSiteID)
        {
            case 12:
                return "http://click2win4life.com/hosting/staticpages/C2W_GameCL.aspx?c=C2WCL&redir=0&clear=1";
            default:
                Console.WriteLine("Default case");
                break;
        }
        return "";
    }


    
}