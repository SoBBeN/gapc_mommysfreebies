﻿
using System.Data.SqlClient;
using System.Text;

namespace DB
{
	public class DB_Functions
	{
		private static int WebSiteID;
		public DB_Functions(int websiteID)
		{
			WebSiteID = websiteID;
		}


		/****************************MasterPage Functions Start**************************/
		public  SqlDataReader GetWebSite()
		{
			SqlServer objSqlServer = new SqlServer();
			StringBuilder query = new StringBuilder("[Website_GetOne] ");
			query.Append(objSqlServer.FormatSql(WebSiteID));
			return objSqlServer.GetDataReader(query.ToString());
		}

		public  SqlDataReader GetContentCategoriesActive()
		{
			SqlServer objSqlServer = new SqlServer();
			StringBuilder query = new StringBuilder("[Content_GetAllCategoriesActive] ");
			query.Append(objSqlServer.FormatSql(WebSiteID));
			return objSqlServer.GetDataReader(query.ToString());
		}

		/*Check what this is To do IS This Needed? What does it do?*/
		public  SqlDataReader GetInternalAdsActive()
		{
			SqlServer objSqlServer = new SqlServer();
			StringBuilder query = new StringBuilder("[InternalAds_GetAllActive] ");
			query.Append(objSqlServer.FormatSql(WebSiteID));

			return objSqlServer.GetDataReader(query.ToString());
		}
		/****************************MasterPage Functions end**************************/
		/****************************Default.cs Functions Start************************/
		public  SqlDataReader GetLifestyle(int id)
		{
			SqlServer objSqlServer = new SqlServer();
			StringBuilder query = new StringBuilder("[Lifestyle_GetList] ");

			query.Append(objSqlServer.FormatSql(id)).Append(",");
			query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
			query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

			return objSqlServer.GetDataReader(query.ToString());
		}

		public SqlDataReader GetNews(int id)
		{
			SqlServer objSqlServer = new SqlServer();
			StringBuilder query = new StringBuilder("[News_GetList] ");

			query.Append(objSqlServer.FormatSql(id)).Append(",");
			query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
			query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

			return objSqlServer.GetDataReader(query.ToString());
		}
		public SqlDataReader GetPointsOffersList(int id)
		{
			SqlServer objSqlServer = new SqlServer();
			StringBuilder query = new StringBuilder("[PointsOffers_GetList] ");

			query.Append(objSqlServer.FormatSql(id)).Append(",");
			query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
			query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

			return objSqlServer.GetDataReader(query.ToString());
		}
		public SqlDataReader GetSweepContestsList(int id, bool isSample = false)
		{
			SqlServer objSqlServer = new SqlServer();
			StringBuilder query = new StringBuilder("[SweepContests_GetList] ");

			query.Append(objSqlServer.FormatSql(id)).Append(",");
			query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
			query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]).Append(",");
			query.Append(objSqlServer.FormatSql(isSample));

			return objSqlServer.GetDataReader(query.ToString());
		}

		public SqlDataReader GetContent(int id)
		{
			SqlServer objSqlServer = new SqlServer();
			StringBuilder query = new StringBuilder("[Content_GetOne] ").Append(objSqlServer.FormatSql(id));

			query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
			return objSqlServer.GetDataReader(query.ToString());
		}

		public SqlDataReader GetLeadData(int leadId)
		{
			SqlServer objSqlServer = new SqlServer();
			StringBuilder query = new StringBuilder("[Partners_GetLeadData2] ");

			SqlConnection conection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Click4RichesConn"].ConnectionString);
			conection.Open();

			query.Append(objSqlServer.FormatSql(leadId));

			return objSqlServer.GetDataReader(query.ToString(), conection);
		}
		/****************************Default.cs Functions End**************************/
	}
}
