﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Recipes : System.Web.UI.Page
{
    private const int NB_PER_PAGE = 6;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        SqlDataReader dr;

        string mycategory = "Recipe Categories";

        dr = DB.DbFunctions.GetRecipeCategoriesRecipe();

        if (dr != null)
        {
            string mylink = string.Empty;

            if (dr.HasRows)
            {
                mylink = "/Recipes/";

                breadcrumbs1.AddLevel(mycategory, mylink);
                ((ITmgMasterPage)Master).PageTitle = mycategory;
                ((ITmgMasterPage)Master).PageURL = mylink;
                ((ITmgMasterPage)Master).PageType = "article";

                int groupby = 10;
                int i = 0;
                StringBuilder sb = new StringBuilder();
                
                int start = 0;
                if (int.TryParse(Request.QueryString["s"], out start) && start >= NB_PER_PAGE)
                {
                    lnkPrev.HRef = "/Recipes/?s=" + (start - NB_PER_PAGE).ToString();
                    lnkPrev.Visible = true;
                }

                lnkNext.HRef = "/Recipes/?s=" + (start + NB_PER_PAGE).ToString();
                while (start-- > 0 && dr.Read());
                while (dr.Read() && i++ < NB_PER_PAGE)
                {
                    if (groupby != Convert.ToInt32(dr["GroupBy"]))
                    {
                        groupby = Convert.ToInt32(dr["GroupBy"]);
                        sb.Append("<br />");
                    }

                    string url = "Recipes/" + dr["ID"] + "/" + Functions.StrToURL(dr["Category"]) + "/" + Functions.StrToURL(dr["Description"]);

                    string link = String.Format("<a href=\"/{0}\">", url);
                    string imageurl = System.Configuration.ConfigurationManager.AppSettings["baseimagesrecipeurl"] + Convert.ToString(dr["ImageFilename"]);
                    string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Description"])), 300).Replace("\"", " ").Trim();

                    sb.Append("<div class=\"lifestyle\">");
                    sb.Append(link).AppendFormat("<div class=\"img recipe\"><img src=\"{0}\"></a></div>", imageurl);
                    sb.Append("<div class=\"description\">");
                    sb.AppendFormat("<div class=\"title\">{1}{0}</a></div>", title, link);
                    sb.Append("</div>");

                    sb.Append("</div><div style=\"clear:both;\"></div><br /><hr class=\"blueline\" />");
                    //if (i % 3 == 0)
                    //{
                    //    if (Request.Browser.IsMobileDevice)
                    //    {
                    //        sb.Append("<div class=\"ad\">");
                    //        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- Free Samples - Bottom --><ins class=\"adsbygoogle\" style=\"display:display:block;width:98%;height:90px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"7335364784\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                    //        sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                    //    }
                    //}
                }

                //if (i < 3)
                //{
                //    if (Request.Browser.IsMobileDevice)
                //    {
                //        sb.Append("<div class=\"ad\">");
                //        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- Free Samples - Bottom --><ins class=\"adsbygoogle\" style=\"display:display:block;width:98%;height:90px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"7335364784\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                //        sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                //    }
                //}

                litArticles.Text = sb.ToString();
                if (i > NB_PER_PAGE)
                    lnkNext.Visible = true;
            }

            dr.Close();
            dr.Dispose();
        }

        dr = DB.DbFunctions.GetContent(97);

        if (dr != null)
        {
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    litDescriptionTop.Text = Functions.ConvertToString(dr["DescriptionTop"]);
                    litDescriptionBtm.Text = Functions.ConvertToString(dr["DescriptionBtm"]);

                    if (dr["ImageFilename"] != null && Convert.ToString(dr["ImageFilename"]).Length > 0)
                    {
                        if (Functions.ConvertToString(dr["ImageLink"]).Length > 0)
                            litImage.Text = "<a href=\"" + Functions.ConvertToString(dr["ImageLink"]) + "\" target=\"_blank\">";
                        else
                            litImage.Text = "<a>";

                        litImage.Text += "<img src=\"" + System.Configuration.ConfigurationManager.AppSettings["baseimagescontentsurl"] + Functions.ConvertToString(dr["ImageFilename"]) + "\" id=\"imgMain\" class=\"img\" />";
                        litImage.Text += "</a>";
                    }
                    else
                        litImage.Visible = false;
                }
            }

            dr.Close();
            dr.Dispose();
        }
    }
}